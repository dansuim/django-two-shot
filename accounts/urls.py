from django.urls import path
from accounts.views import user_login, user_logout, signup


urlpatterns = [
    path("login/", user_login, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup, name="signup"),
    # path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    # path("create/", todo_list_create, name="todo_list_create"),
]
