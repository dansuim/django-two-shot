from django.urls import path
from receipts.views import expense_receipts_receipt, create_receipt, category_list, account_list,create_category, create_account


urlpatterns = [
    path("", expense_receipts_receipt, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("categories/", category_list, name="category_list"),
    path("accounts/", account_list, name="account_list"),
    path("accounts/create/", create_account, name="create_account"),
    path("categories/create/", create_category, name="create_category"),
    # path("<int:id>/", todo_list_detail, name="todo_list_detail"),
    # path("create/", todo_list_create, name="todo_list_create"),
]
