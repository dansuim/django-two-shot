from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, ExpenseCategoryForm, AccountForm


# Create your views here.
# Receipts Model filtered to just user receipts
@login_required
def expense_receipts_receipt(request):
    receipts_receipt = Receipt.objects.filter(purchaser=request.user)
    context = {
        "expense_receipts_receipt": receipts_receipt,
    }
    return render(request, "receipts/list.html", context)


# Get all Receipts
# @login_required
# def expense_receipts_receipt(request):
#     receipts_receipt = Receipt.objects.all()
#     context = {
#         "expense_receipts_receipt": receipts_receipt,
#     }
#     return render(request, "receipts/list.html", context)


# create a Receipt
@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()

    context = {
        "form": form,
    }
    return render(request, "receipts/create.html", context)


# Categories list
@login_required
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "categories/list.html", context)


# create a Category
@login_required
def create_category(request):
    if request.method == "POST":
        form = ExpenseCategoryForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("category_list")
    else:
        form = ExpenseCategoryForm()

    context = {
        "form": form,
    }
    return render(request, "categories/create.html", context)


# Accounts list
@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "accounts/list.html", context)


# create an Account
@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.owner = request.user
            receipt.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form": form,
    }
    return render(request, "accounts/create.html", context)
